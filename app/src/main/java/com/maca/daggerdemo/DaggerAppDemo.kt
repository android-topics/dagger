package com.maca.daggerdemo

import android.app.Application
import android.content.Context
import com.maca.daggerdemo.di.AppComponent
import com.maca.daggerdemo.di.DaggerAppComponent

class DaggerAppDemo : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        // 3
        appComponent = DaggerAppComponent
            .builder()
            .bindApplication(this)
            .build()
    }
}


val Context.appComponent: AppComponent
    get() = (applicationContext as DaggerAppDemo).appComponent