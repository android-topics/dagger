package com.maca.daggerdemo.ui.dashboard

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.maca.daggerdemo.databinding.FragmentDashboardBinding
import com.maca.daggerdemo.di.DashBoardQualifier
import com.maca.daggerdemo.ui.MainActivity
import javax.inject.Inject

class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @Inject
    @DashBoardQualifier
    lateinit var dashboardFragmentString: String

    override fun onAttach(context: Context) {
        (activity as? MainActivity)?.activityComponent
            ?.fragmentComponent()
            ?.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textDashboard

        textView.text = dashboardFragmentString

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}