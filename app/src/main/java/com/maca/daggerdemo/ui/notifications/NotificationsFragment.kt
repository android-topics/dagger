package com.maca.daggerdemo.ui.notifications

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.maca.daggerdemo.databinding.FragmentNotificationsBinding
import com.maca.daggerdemo.di.NotificationsQualifier
import com.maca.daggerdemo.ui.MainActivity
import javax.inject.Inject

class NotificationsFragment : Fragment() {

    private var _binding: FragmentNotificationsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    @Inject
    @NotificationsQualifier
    lateinit var notificationFragmentString: String

    override fun onAttach(context: Context) {
        (activity as? MainActivity)?.activityComponent
            ?.fragmentComponent()
            ?.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textNotifications

        textView.text = notificationFragmentString

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}