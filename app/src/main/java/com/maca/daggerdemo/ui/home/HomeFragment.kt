package com.maca.daggerdemo.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.maca.daggerdemo.databinding.FragmentHomeBinding
import com.maca.daggerdemo.di.HomeFragmentQualifier
import com.maca.daggerdemo.ui.MainActivity
import javax.inject.Inject

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @Inject
    @HomeFragmentQualifier
    lateinit var homeFragmentString: String

    override fun onAttach(context: Context) {
        (activity as? MainActivity)?.activityComponent
            ?.fragmentComponent()
            ?.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textHome
        textView.text = homeFragmentString

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}