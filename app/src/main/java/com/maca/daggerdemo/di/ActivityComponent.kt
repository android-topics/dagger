package com.maca.daggerdemo.di

import android.app.Activity
import com.maca.daggerdemo.ui.MainActivity
import dagger.BindsInstance
import dagger.Subcomponent
import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope


@Subcomponent
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

    fun fragmentComponent(): FragmentComponent

    @Subcomponent.Builder
    interface Builder {

        fun bindCurrentActivity(
            @BindsInstance
            activity: Activity
        ): Builder

        fun build(): ActivityComponent
    }
}