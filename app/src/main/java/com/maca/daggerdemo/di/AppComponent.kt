package com.maca.daggerdemo.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope


@Component(modules = [AppModule::class])
@ApplicationScope
interface AppComponent {
    fun activityComponentBuilder(): ActivityComponent.Builder

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindApplication(application: Application): Builder

        fun build(): AppComponent

    }
}
