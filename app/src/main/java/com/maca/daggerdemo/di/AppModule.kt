package com.maca.daggerdemo.di

import android.app.Application
import dagger.Module
import dagger.Provides

@Module
class AppModule {

//    @Provides
//    @ApplicationScope
//    fun providesString(): Int = 1

    @Provides
    @ApplicationScope
    fun providesAppGenericString(application: Application): String =
        "generic string of ${application.packageName}"
}