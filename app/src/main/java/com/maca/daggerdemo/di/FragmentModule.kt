package com.maca.daggerdemo.di

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope


@Module
class FragmentModule {

    @Provides
    @FragmentScope
    @HomeFragmentQualifier
    fun providesStringForHomeFragment(): String = "Welcome to your homescreen!"

    @Provides
    @FragmentScope
    @NotificationsQualifier
    fun providesStringForNotificationFragment(): String =
        "In this fragment you can get your notifications!"

    @Provides
    @FragmentScope
    @DashBoardQualifier
    fun providesStringForDashboardFragment(application: Application): String =
        "This is the dashboard of the application with package: ${application.packageName} " +
                "and reference $application"
}