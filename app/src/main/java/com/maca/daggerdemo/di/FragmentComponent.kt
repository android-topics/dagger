package com.maca.daggerdemo.di

import com.maca.daggerdemo.ui.dashboard.DashboardFragment
import com.maca.daggerdemo.ui.home.HomeFragment
import com.maca.daggerdemo.ui.notifications.NotificationsFragment
import dagger.Subcomponent


@Subcomponent(modules = [FragmentModule::class])
@FragmentScope
interface FragmentComponent {

    fun inject(fragment: DashboardFragment)
    fun inject(fragment: HomeFragment)
    fun inject(fragment: NotificationsFragment)
}