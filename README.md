# Dagger showcase.
Definition from RWL book:

`@Components` describes which objects in the dependency graph you can inject, along with all their dependencies

`@Module` tells dagger how to create the objects in the dependency graph

In the following examples:

- T is a class that implements the interface V

- P is a class that implements the interface V


### Scoping Components

To scope a compo
- @Scope annotation: 
    ``` kotlin
    @Scope
    @MustBeDocumented
    @Retention(RUNTIME)
    annotation class ActivityScope
    ```

    On the Component:

    ``` kotlin
    @ActivityScope
    interface ActivityComponent {
        ...
    }
    ```

    On the module: 

    ``` kotlin
    @Module(includes = [...])
    class ActivityModule {
        ...
        @Provides
        @ActivityScope
        fun provideNavigator(activity: Activity): Navigator = NavigatorImpl(activity)
    }
    ```

### Managing dependencies between objects
There is a two main ways to manage relationship between component objects.

Suppose that your app got 2 components: `ApplicationComponent` and `ActivityComponent`, now suppose that an `ApiEndpoint` built with retrofit is provided on `ApplicationComponent`, what if `ActivityComponent` needs that instance of `ApiEndpoint`:
- Using only components:
 TODO
- Using subcomponents:
 TODO

### Providing objects to the graph

To populate the graph with the required dependencies you can use:
- @Provides annotation:
    ``` kotlin
    @Provides 
    fun providesTInstance(): V = T()

    ```

- @Binds annotation: 
    on an abstract/interface module:
    ``` kotlin
    @Binds
    fun bindsTInstance(impl: T): V  
    ```

    Modify the T constructor
    ``` kotlin
    class T @Inject constructor( ... ): V{
        ...
    }
    ```
### Binding objects when creating the graph:

If you want to give your activity/application context on the graph for example, you can bind it in two ways on the component:

- Builder

    ``` kotlin 

    @Singleton
    @Component(modules = [...])
    interface AppComponent {
        @Component.Builder
        interface Builder {
            @BindsInstance
            fun activity(activity: Activity): Builder

            fun buildComponent(): AppComponent
        }
    }
    ```
    Then, you can use it as follows: 
    ```kotlin
    DaggerAppComponent
        .builder()
        .activity(this)
        .buildComponent().also {
            it.inject(this)
      }

    ```
- Factory

    ``` kotlin 

    @Singleton
    @Component(modules = [...])
    interface AppComponent {
        
        @Component.Factory
        interface Factory {

            fun create(@BindsInstance activity: Activity): AppComponent
        }
    }
    ```
    Then, you can use it as follows: 
    ```kotlin
    DaggerAppComponent
        .factory()
        .create(this).also { 
          it.inject(this)
      }
    ```

### Managing T is bound multiple times:

If you got multiple instances of V, you have to specify to Dagger which one to use:


Let's suppose that you want to provide the T class instead of the P class in this component.
- @Named annotation:
    On the file where you are providing the V implementation that you want to use:
    ``` kotlin
    @Binds
    @Named("CustomT")
    fun bindTObject(myInstance: T): V
    ```
    
    On the file that you are injecting V.

    ``` kotlin
    @Inject
    @Named("CustomT")
    lateinit var myInstanceT: V
    ```

- Custom Qualifiers.
    First, you have to define the qualifier like this: 
    
    ``` kotlin
    @Qualifier
    @MustBeDocumented
    @Retention(AnnotationRetention.RUNTIME)
    annotation class MyTClass
    ```

    On the file where you are providing the V implementation that you want to use:
    
    ``` kotlin
    @Binds
    @TClass
    fun bindTObject(myInstance: T): V
    ```
    
    On the file that you are injecting V.
    
    ``` kotlin
    @Inject
    @TClass
    lateinit var myInstanceT: V
    ```

### Relevant notes:
- “By default, Dagger creates a new instance of the class that’s bound to a specific abstraction type every time an injection of that type occurs.
Using @Singleton, you can only bind the lifecycle of an instance to the lifecycle of the @Component you use to get its reference.
In other words, a @Singleton IS NOT a Singleton!
Using @Singleton doesn’t give you a unique instance of a class across the entire app. It just means that each instance of a @Component always returns the same instance for an object of a given type. Different @Component instances will return different instances for the same type, even if you use @Singleton.”

Excerpt From: By Massimo Carli. “Dagger by Tutorials.” Apple Books. 

- Custom Qualifiers are less error prone than the Named annotation, if you go for @Named annotation please considere to use a compilation time constante to define the name i.e: `const val CUSTOM_T = "CustomT"`

- Sometimes dagger compiler throws very generic errors so try to run: ./gradlew assembleDebug --stacktrace in order to see what is going on.
